import './App.css';
import { useState } from 'react';

function App() {

  /** States */
  const [displayValue, setDisplayValue] = useState('0');
  const [clearDisplay, setClearDisplay] = useState(false);
  const [operation, setOperation] = useState(null);
  const [values, setValues] = useState([0,0]);
  const [current, setCurrent] = useState(0);

  const addDigit = event => {
   const n = event.target.value;
    const cDisplay = displayValue === '0' || clearDisplay;

    if (n === '.' && !cDisplay && displayValue.includes('.')) {
      return;
    }    
    
    const currentValue = cDisplay ? '' : displayValue;
    const dValue = currentValue + n;

    setDisplayValue(dValue);
    setClearDisplay(false);

    if(n !== '.') {
      const newValue = parseFloat(dValue)
      const vals = [...values]
      vals[current] = newValue
      setValues(vals);
    }
  }

  const clearMemory = () => {
    setDisplayValue('0');
    setClearDisplay(false);
    setOperation(null);
    setValues([0,0]);
    setCurrent(0);
  }

  const handleOperation = event => {
    const op = event.target.value;
    if(current === 0) {
      setOperation(op);
      setCurrent(1);
      setClearDisplay(true);
    } else {
      const equals = op === '=';
      const v = [...values];
      try {
        v[0] = eval(`${v[0]} ${operation} ${v[1]}`)
      } catch (e) {
        v[0] = values[0]
      }

      v[1] = 0;
      setDisplayValue(`${v[0]}`);
      setOperation(equals ? null : op);
      setCurrent(equals ? 0 : 1);
      setClearDisplay(true);
      setValues(v);
    }
  }


  return (
    <>
      <div className="display">
      <input className="displayValue" value={displayValue} />
      <div className="buttons">
        <button value={7} onClick={addDigit}>7</button>
        <button value={8} onClick={addDigit}>8</button>
        <button value={9} onClick={addDigit}>9</button>
        <button className="buttonOp" value={'*'} operation onClick={handleOperation}>*</button>
        <button value={4} onClick={addDigit}>4</button>
        <button value={5} onClick={addDigit}>5</button>
        <button value={6} onClick={addDigit}>6</button>
        <button className="buttonOp" value={'-'} operation onClick={handleOperation}> - </button>   
        <button value={1} onClick={addDigit}>1</button>
        <button value={2} onClick={addDigit}>2</button>
        <button value={3} onClick={addDigit}>3</button>
        <button className="buttonOp" value={'+'} operation onClick={handleOperation}>+</button>
        <button value={'.'} onClick={addDigit}>.</button>
        <button value={0} onClick={addDigit}>0</button>
        <button value={'='} operation onClick={handleOperation}> = </button>
        <button className="buttonOp" value={'/'} operation onClick={handleOperation}>/</button>
        <button className="buttonOp" onClick={clearMemory}>AC</button>
      </div>
      </div>
   </>
  );
}

export default App;
